#!/bin/bash

cd /sources
tar -xpf man-pages-5.13.tar.xz
cd man-pages-5.13
make prefix=/usr install && cd ..
rm -Rf man-pages
tar -xpf tcl8.6.11-src.tar.gz
cd tcl8.6.11
tar -xf ../tcl8.6.11-html.tar.gz --strip-comments=1
SRCDIR=$(pwd)
cd unix
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            $([ "$(uname -m)" = x86_64 ] && echo --enable-64bit)
make

sed -e "s|$SRCDIR/unix|/usr/lib|" \
    -e "s|$SRCDIR|/usr/include|"  \
    -i tclConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.1|/usr/lib/tdbc1.1.1|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1/library|/usr/lib/tcl8.6|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1|/usr/include|"            \
    -i pkgs/tdbc1.1.1/tdbcConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.0|/usr/lib/itcl4.2.0|" \
    -e "s|$SRCDIR/pkgs/itcl4.2.0/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/itcl4.2.0|/usr/include|"            \
    -i pkgs/itcl4.2.0/itclConfig.sh

unset SRCDIR
make install
chmod -v u+w /usr/lib/libtcl8.6.so
make install-private-headers
cd .. && rm -Rf tcl8.6.11
tar -xpf expect5.45.4.tar.gz
cd expect5.45.5
./configure --prefix=/usr           \
            --with-tcl=/usr/lib     \
            --enable-shared         \
            --mandir=/usr/share/man \
            --with-tclinclude=/usr/include && make && make install && ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib
cd .. && rm -Rf expect5.45.4
tar xpf dejagnu-1.6.3.tar.gz
cd dejagnu-1.6.3
./configure --prefix=/usr
makeinfo --html --no-split -o doc/dejagnu.html doc/dejagnu.texi
makeinfo --plaintext       -o doc/dejagnu.txt  doc/dejagnu.texi
make install
install -v -dm755  /usr/share/doc/dejagnu-1.6.2
install -v -m644   doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.3
cd .. && rm -rf dejagnu-1.6.3
tar xpf iana-etc-20211112.tar.gz
cd iana-etc-20211112
cp services protocols /etc/
cd .. && rm -Rf iana-etc-20211112
tar xpf glibc-2.34.tar.xz
cd glibc-2.34
patch -Np1 -i ../glibc-2.34-fhs-1.patch
mkdir -v build
cd       build
../configure --prefix=/usr                            \
             --disable-werror                         \
             --enable-kernel=3.2                      \
             --enable-stack-protector=strong          \
             --with-headers=/usr/include              \
             --enable-multi-arch                      \
             libc_cv_slibdir=/lib
make && case $(uname -m) in
  i?86)   ln -sfnv $PWD/elf/ld-linux.so.2        /lib ;;
  x86_64) ln -sfnv $PWD/elf/ld-linux-x86-64.so.2 /lib ;;
esac
touch /etc/ld.so.conf
sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
make install
cp -v ../nscd/nscd.conf /etc/nscd.conf
mkdir -pv /var/cache/nscd
mkdir -pv /usr/lib/locale
localedef -i en_US -f UTF-8 en_US.UTF-8
cp -r files/etc/nsswitch.conf /etc/
tar -xf ../../tzdata2020a.tar.gz

ZONEINFO=/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}

for tz in etcetera southamerica northamerica europe africa antarctica  \
          asia australasia backward pacificnew systemv; do
    zic -L /dev/null   -d $ZONEINFO       ${tz}
    zic -L /dev/null   -d $ZONEINFO/posix ${tz}
    zic -L leapseconds -d $ZONEINFO/right ${tz}
done

cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO
ln -sf /usr/share/zoneinfo/America/Louisville /etc/localtime

cp /files/etc/ld.so.conf /etc/
mkdir -pv /etc/ld.so.conf.d
rm -rf ./*
find .. -name "*.a" -delete
CC="gcc -m32" CXX="g++ -m32" \
../configure                             \
      --prefix=/usr                      \
      --host=i686-pc-linux-gnu           \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=/usr/include        \
      --enable-multi-arch                \
      --libdir=/usr/lib32                \
      --libexecdir=/usr/lib32            \
      libc_cv_slibdir=/lib32
make && make DESTDIR=$PWD/DESTDIR install
cp -a DESTDIR/lib32/*     /lib32/
cp -a DESTDIR/usr/lib32/* /usr/lib32/
install -vm644 DESTDIR/usr/include/gnu/{lib-names,stubs}-32.h \
               /usr/include/gnu/
ln -svf ../lib32/ld-linux.so.2 /lib/ld-linux.so.2
echo "/usr/lib32" >> /etc/ld.so.conf
rm -rf ./*
find .. -name "*.a" -delete
CC="gcc -mx32" CXX="g++ -mx32" \
../configure                             \
      --prefix=/usr                      \
      --host=x86_64-pc-linux-gnux32      \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=$LFS/usr/include    \
      --enable-multi-arch                \
      --libdir=/usr/libx32               \
      --libexecdir=/usr/libx32           \
      libc_cv_slibdir=/libx32 && make && make DESTDIR=$PWD/DESTDIR install
cp -a DESTDIR/libx32/*     /libx32/
cp -a DESTDIR/usr/libx32/* /usr/libx32/
install -vm644 DESTDIR/usr/include/gnu/{lib-names,stubs}-x32.h \
               /usr/include/gnu/
ln -svf ../libx32/ld-linux-x32.so.2 /lib/ld-linux-x32.so.2
echo "/usr/libx32" >> /etc/ld.so.conf
cd .. && rm -rf glibc-2.34
tar -xpf zlib-1.2.11.tar.xz
cd zlib-1.2.11
./configure --prefix=/usr
make && make install && rm -fv /usr/lib/libz.a
make distclean
CC="gcc -m32" \
./configure --prefix=/usr \
    --libdir=/usr/lib32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" \
./configure --prefix=/usr    \
    --libdir=/usr/libx32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf zlib-1.2.11
tar xpf bzip2-1.0.8.tar.gz && cd bzip2-1.0.8
patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
make -f Makefile-libbz2_so
make clean
make && make PREFIX=/usr install
cp -av libbz2.so.* /usr/lib
ln -sv libbz2.so.1.0.8 /usr/lib/libbz2.so
cp -v bzip2-shared /usr/bin/bzip2
for i in /usr/bin/{bzcat,bunzip2}; do
  ln -sfv bzip2 $i
done
rm -fv /usr/lib/libbz2.a
make clean
sed -e "s/^CC=.*/CC=gcc -m32/" -i Makefile{,-libbz2_so}
make -f Makefile-libbz2_so
make libbz2.a
install -Dm755 libbz2.so.1.0.8 /usr/lib32/libbz2.so.1.0.8
ln -sf libbz2.so.1.0.8 /usr/lib32/libbz2.so
ln -sf libbz2.so.1.0.8 /usr/lib32/libbz2.so.1
ln -sf libbz2.so.1.0.8 /usr/lib32/libbz2.so.1.0
install -Dm644 libbz2.a /usr/lib32/libbz2.a
make clean
sed -e "s/^CC=.*/CC=gcc -mx32/" -i Makefile{,-libbz2_so}
make -f Makefile-libbz2_so
make libbz2.a
install -Dm755 libbz2.so.1.0.8 /usr/libx32/libbz2.so.1.0.8
ln -sf libbz2.so.1.0.8 /usr/libx32/libbz2.so
ln -sf libbz2.so.1.0.8 /usr/libx32/libbz2.so.1
ln -sf libbz2.so.1.0.8 /usr/libx32/libbz2.so.1.0
install -Dm644 libbz2.a /usr/libx32/libbz2.a
cd .. && rm -Rf bzip2-1.0.8
tar -xpf xz-5.2.5.tar.xz
cd xz-5.2.5 && ./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/xz-5.2.5 && make && make install
make distclean
CC="gcc -m32" ./configure \
    --host=i686-pc-linux-gnu      \
    --prefix=/usr                 \
    --libdir=/usr/lib32           \
    --disable-static && make && make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" ./configure \
    --host=x86_64-pc-linux-gnux32 \
    --prefix=/usr                 \
    --libdir=/usr/libx32          \
    --disable-static && make && make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf xz-5.2.5
tar -xpf zstd-1.5.0.tar.gz && cd zstd-1.5.0
make && make prefix=/usr install
rm -v /usr/lib/libzstd.a
make clean && CC="gcc -m32" make
make prefix=/usr DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib/* /usr/lib32/
sed -e "/^libdir/s/lib$/lib32/" -i /usr/lib32/pkgconfig/libzstd.pc
rm -rf DESTDIR && make clean
CC="gcc -mx32" make
make prefix=/usr DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib/* /usr/libx32/
sed -e "/^libdir/s/lib$/libx32/" -i /usr/libx32/pkgconfig/libzstd.pc
rm -rf DESTDIR && cd .. && rm -Rf zstd-1.5.0
tar xpf file-5.41.tar.gz && cd file-5.41
./configure --prefix=/usr && make && make install
make distclean && CC="gcc -m32" ./configure \
    --prefix=/usr         \
    --libdir=/usr/lib32   \
    --host=i686-pc-linux-gnu && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" ./configure \
    --prefix=/usr          \
    --libdir=/usr/libx32   \
    --host=x86_64-pc-linux-gnux32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf file-5.41
tar -xpf readline-8.1.tar.gz && cd readline-8.1
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install
./configure --prefix=/usr    \
            --disable-static \
            --with-curses    \
            --docdir=/usr/share/doc/readline-8.1
make SHLIB_LIBS="-lncursesw" && make SHLIB_LIBS="-lncursesw" install
make distclean
CC="gcc -m32" ./configure \
    --host=i686-pc-linux-gnu      \
    --prefix=/usr                 \
    --libdir=/usr/lib32           \
    --disable-static && make SHLIB_LIBS="-lncursesw"
make SHLIB_LIBS="-lncursesw" DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" ./configure \
    --host=x86_64-pc-linux-gnux32 \
    --prefix=/usr                 \
    --libdir=/usr/libx32          \
    --disable-static && make SHLIB_LIBS="-lncursesw"
make SHLIB_LIBS="-lncursesw" DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf readline-8.1
tar xpf m4-1.4.19.tar.xz && cd m4-1.4.19
./configure --prefix=/usr && make && make install
cd .. && rm -Rf m4-1.4.19
tar xf bc-5.2.1.tar.xz && cd bc-5.2.1
CC=gcc ./configure --prefix=/usr -G -O3 && make && make install
cd .. && Rm -rf bc-5.2.1
tar xf flex-2.6.4.tar.gz && cd flex-2.6.4
./configure --prefix=/usr \
            --docdir=/usr/share/doc/flex-2.6.4 \
            --disable-static && make && make install && ln -sv flex /usr/bin/lex
cd .. && rm -Rf flex-2.6.4
tar xf tcl-8.6.11-src.tar.gz && cd tcl8.6.11
tar -xf ../tcl8.6.11-html.tar.gz --strip-components=1
SRCDIR=$(pwd)
cd unix
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            $([ "$(uname -m)" = x86_64 ] && echo --enable-64bit) &&
make

sed -e "s|$SRCDIR/unix|/usr/lib|" \
    -e "s|$SRCDIR|/usr/include|"  \
    -i tclConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.2|/usr/lib/tdbc1.1.2|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2/library|/usr/lib/tcl8.6|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.2|/usr/include|"            \
    -i pkgs/tdbc1.1.2/tdbcConfig.sh

sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.1|/usr/lib/itcl4.2.1|" \
    -e "s|$SRCDIR/pkgs/itcl4.2.1/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/itcl4.2.1|/usr/include|"            \
    -i pkgs/itcl4.2.1/itclConfig.sh

unset SRCDIR
make install
chmod -v u+w /usr/lib/libtcl8.6.so
make install-private-headers
ln -sfv tclsh8.6 /usr/bin/tclsh
mv /usr/share/man/man3/{Thread,Tcl_Thread}.3
cd .. && cd .. && rm -Rf tcl8.6.11
tar -xpf expect5.45.4.tar.gz && cd expect5.45.4
./configure --prefix=/usr           \
            --with-tcl=/usr/lib     \
            --enable-shared         \
            --mandir=/usr/share/man \
            --with-tclinclude=/usr/include && make && make install
ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib && cd .. && rm -Rf expect5.45.4
tar -xpf dejagnu-1.6.3 && cd dejagnu-1.6.3
mkdir -v build && cd build
../configure --prefix=/usr
makeinfo --html --no-split -o doc/dejagnu.html ../doc/dejagnu.texi
makeinfo --plaintext       -o doc/dejagnu.txt  ../doc/dejagnu.texi &&
make install
install -v -dm755  /usr/share/doc/dejagnu-1.6.3
install -v -m644   doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.3
cd .. && rm -Rf dejagnu-1.6.3
tar -xvpf binutils-2.37.tar.xz && cd binutils-2.37
expect -c "spawn ls"
patch -Np1 -i ../binutils-2.37-upstream_fix-1.patch
sed -i '63d' etc/texi2pod.pl
find -name \*.1 -delete
mkdir -v build
cd       build
../configure --prefix=/usr       \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bfd \
             --with-system-zlib  \
             --enable-multilib && make tooldir=/usr && make tooldir=/usr install -j1
rm -fv /usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a
cd .. && cd .. && rm -Rf binutils-2.37
tar xpf gpm-6.2.1 && cd gpm-6.2.1
./configure --prefix=/usr    \
            --enable-cxx     \
            --disable-static \
            --docdir=/usr/share/doc/gmp-6.2.1 && make && make html && make install && make install-html

make distclean
cp -v configfsf.guess config.guess
cp -v configfsf.sub   config.sub
ABI="32" \
CFLAGS="-m32 -O2 -pedantic -fomit-frame-pointer -mtune=generic -march=i686" \
CXXFLAGS="$CFLAGS" \
PKG_CONFIG_PATH="/usr/lib32/pkgconfig" \
./configure                      \
    --host=i686-pc-linux-gnu     \
    --prefix=/usr                \
    --disable-static             \
    --enable-cxx                 \
    --libdir=/usr/lib32          \
    --includedir=/usr/include/m32/gmp && sed -i 's/$(exec_prefix)\/include/$\(includedir\)/' Makefile
make

make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
cp -Rv DESTDIR/usr/include/m32/* /usr/include/m32/
rm -rf DESTDIR && make distclean
cp -v configfsf.guess config.guess
cp -v configfsf.sub   config.sub
ABI="x32" \
CFLAGS="-mx32 -O2 -pedantic -fomit-frame-pointer -mtune=generic -march=x86-64" \
CXXFLAGS="$CFLAGS" \
PKG_CONFIG_PATH="/usr/libx32/pkgconfig" \
./configure                       \
    --host=x86_64-pc-linux-gnux32 \
    --prefix=/usr                 \
    --disable-static              \
    --enable-cxx                  \
    --libdir=/usr/libx32          \
    --includedir=/usr/include/mx32/gmp && sed -i 's/$(exec_prefix)\/include/$\(includedir\)/' Makefile
make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
cp -Rv DESTDIR/usr/include/mx32/* /usr/include/mx32/
rm -rf DESTDIR && cd .. && rm -rf gmp-6.2.1
tar xpf mpfr-4.1.0.tar.xz && cd mpfr-4.1.0
./configure --prefix=/usr        \
            --disable-static     \
            --enable-thread-safe \
            --docdir=/usr/share/doc/mpfr-4.1.0 && make
make html && make install && make install-html && cd .. && rm -Rf mpfr-4.1.0
tar -xpf mpc-1.2.1.tar.gz && cd mpc-1.2.1
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/mpc-1.2.1 && make && make html
make install && make install-html && cd .. && rm -Rf mpc-1.2.1
tar xpf isl-0.22.1 && cd isl-0.22.1

./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/isl-0.22.1 && make
make install
install -vd /usr/share/doc/isl-0.22.1
install -m644 doc/{CodingStyle,manual.pdf,SubmittingPatches,user.pod} \
        /usr/share/doc/isl-0.22.1
mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/libisl*gdb.py /usr/share/gdb/auto-load/usr/lib && cd .. && rm -rf isl-0.22.1
tar -xpf attr-2.5.1.tar.gz && cd attr-2.5.1
./configure --prefix=/usr     \
            --disable-static  \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/attr-2.5.1 && make && make install
make distclean

CC="gcc -m32" ./configure \
    --prefix=/usr         \
    --disable-static      \
    --libdir=/usr/lib32 \
    --host=i686-pc-linux-gnu && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" ./configure \
    --prefix=/usr          \
    --disable-static       \
    --libdir=/usr/libx32   \
    --host=x86_64-pc-linux-gnux32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && rm -rf attr-2.5.1
tar -xpf acl-2.3.1.tar.xz && cd attr-2.3.1

./configure --prefix=/usr         \
            --disable-static      \
            --docdir=/usr/share/doc/acl-2.3.1 && make && make install
make distclean
CC="gcc -m32" ./configure \
    --prefix=/usr         \
    --disable-static      \
    --libdir=/usr/lib32   \
    --libexecdir=/usr/lib32   \
    --host=i686-pc-linux-gnu && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" ./configure \
    --prefix=/usr          \
    --disable-static       \
    --libdir=/usr/libx32   \
    --libexecdir=/usr/libx32   \
    --host=x86_64-pc-linux-gnux32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf acl-2.3.1
tar -xfp libcap-2.61.tar.xz && cd libcap-2.61
sed -i '/install -m.*STA/d' libcap/Makefile
make prefix=/usr lib=lib && make prefix=/usr /lib=lib install
make distclean
make CC="gcc -m32 -march=i686"
make CC="gcc -m32 -march=i686" lib=lib32 prefix=$PWD/DESTDIR/usr -C libcap install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
sed -e "s|^libdir=.*|libdir=/usr/lib32|" -i /usr/lib32/pkgconfig/lib{cap,psx}.pc
chmod -v 755 /usr/lib32/libcap.so.2.61
rm -rf DESTDIR && make distclean
make CC="gcc -mx32 -march=x86-64"
make CC="gcc -mx32 -march=x86-64" lib=libx32 prefix=$PWD/DESTDIR/usr -C libcap install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
sed -e "s|^libdir=.*|libdir=/usr/libx32|" -i /usr/libx32/pkgconfig/lib{cap,psx}.pc
chmod -v 755 /usr/libx32/libcap.so.2.61
rm -rf DESTDIR && cd .. && rm -rf libcap-2.61
tar xf shadow-4.9 && cd shadow-4.9
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;
sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
    -e 's:/var/spool/mail:/var/mail:'                 \
    -e '/PATH=/{s@/sbin:@@;s@/bin:@@}'                \
    -i etc/login.defs
sed -e "224s/rounds/min_rounds/" -i libmisc/salt.c

touch /usr/bin/passwd
./configure --sysconfdir=/etc \
            --with-group-name-max-length=32 && make
make exec_prefix=/usr install
make -C man install-man
mkdir -p /etc/default
useradd -D --gid 999
pwconv && grpconv
cd .. && rm -Rf shadow-4.9
tar xpf gcc-11.2.0.tar.xz && cd gcc-11.2.0
sed -e '/static.*SIGSTKSZ/d' \
    -e 's/return kAltStackSize/return SIGSTKSZ * 4/' \
    -i libsanitizer/sanitizer_common/sanitizer_posix_libcdep.cpp
sed -e '/m64=/s/lib64/lib/' \
    -e '/m32=/s/m32=.*/m32=..\/lib32$(call if_multiarch,:i386-linux-gnu)/' \
    -i.orig gcc/config/i386/t-linux64
mkdir -v build && cd build
mlist=m64,m32,mx32
../configure --prefix=/usr               \
             LD=ld                       \
             --enable-languages=c,c++    \
             --enable-multilib           \
             --with-multilib-list=$mlist \
             --disable-bootstrap         \
             --with-system-zlib && make -j8

make install
rm -rf /usr/lib/gcc/$(gcc -dumpmachine)/11.2.0/include-fixed/bits/
chown -v -R root:root \
    /usr/lib/gcc/*linux-gnu/11.2.0/include{,-fixed}
ln -svr /usr/bin/cpp /usr/lib
ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/11.2.0/liblto_plugin.so \
        /usr/lib/bfd-plugins/

mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib
cd ../.. && rm -Rf gcc-11.2.0
tar -xpf pkg-config-0.29.2.tar.gz && cd pkg-config-0.29.2
./configure --prefix=/usr              \
            --with-internal-glib       \
            --disable-host-tool        \
            --docdir=/usr/share/doc/pkg-config-0.29.2 && make && make install
cd .. && rm -Rf pkg-config-0.29.2
tar xpf ncurses-6.3.tar.gz && cd ncurses-6.3
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec          \
            --with-pkg-config-libdir=/usr/lib/pkgconfig && make 
make DESTDIR=$PWD/dest install
install -vm755 dest/usr/lib/libncursesw.so.6.3 /usr/lib
rm -v  dest/usr/lib/{libncursesw.so.6.3,libncurses++w.a}
cp -av dest/* /
for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done
rm -vf                     /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so      /usr/lib/libcurses.so && make distclean

CC="gcc -m32" CXX="g++ -m32" \
./configure --prefix=/usr           \
            --host=i686-pc-linux-gnu \
            --libdir=/usr/lib32     \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec          \
            --with-pkg-config-libdir=/usr/lib32/pkgconfig && make
make DESTDIR=$PWD/DESTDIR install
mkdir -p DESTDIR/usr/lib32/pkgconfig
for lib in ncurses form panel menu ; do
    rm -vf                    DESTDIR/usr/lib32/lib${lib}.so
    echo "INPUT(-l${lib}w)" > DESTDIR/usr/lib32/lib${lib}.so
    ln -svf ${lib}w.pc        DESTDIR/usr/lib32/pkgconfig/$lib.pc
done
rm -vf                     DESTDIR/usr/lib32/libcursesw.so
echo "INPUT(-lncursesw)" > DESTDIR/usr/lib32/libcursesw.so
ln -sfv libncurses.so      DESTDIR/usr/lib32/libcurses.so
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" CXX="g++ -mx32" \
./configure --prefix=/usr           \
            --host=x86_64-pc-linux-gnux32 \
            --libdir=/usr/libx32    \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec          \
            --with-pkg-config-libdir=/usr/libx32/pkgconfig && make
make DESTDIR=$PWD/DESTDIR install
mkdir -p DESTDIR/usr/libx32/pkgconfig
for lib in ncurses form panel menu ; do
    rm -vf                    DESTDIR/usr/libx32/lib${lib}.so
    echo "INPUT(-l${lib}w)" > DESTDIR/usr/libx32/lib${lib}.so
    ln -svf ${lib}w.pc        DESTDIR/usr/libx32/pkgconfig/$lib.pc
done
rm -vf                     DESTDIR/usr/libx32/libcursesw.so
echo "INPUT(-lncursesw)" > DESTDIR/usr/libx32/libcursesw.so
ln -sfv libncurses.so      DESTDIR/usr/libx32/libcurses.so
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf ncurses-6.3
tar -xpf sed-4.8.tar.xz && cd sed-4.8
./configure --prefix=/usr && make && make html
make install
install -d -m755           /usr/share/doc/sed-4.8
install -m644 doc/sed.html /usr/share/doc/sed-4.8
cd .. && rm -Rf sed-4.8
tar -xpf psmisc-23.4.tar.xz && cd psmisc-23.4
./configure --prefix=/usr && make && make install
cd .. && rm -rf psmisc-23.4
tar -xpf gettext-0.21.tar.xz && cd gettext-0.21
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.21 && make
make install
chmod -v 0755 /usr/lib/preloadable_libintl.so && cd .. && rm -Rf gettext-0.21
tar -xpf bison-3.8.2.tar.xz && cd bison-3.8.2
./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.8.2
make && make install && cd .. && rm -Rf bison-3.8.2
tar -xpf grep-3.7.tar.xz && cd grep-3.7
./configure --prefix=/usr && make && make install
cd .. && rm -Rf grep-3.7
tar -xpf bash-5.1.8 && cd bash-5.1.8

./configure --prefix=/usr                      \
            --docdir=/usr/share/doc/bash-5.1.8 \
            --without-bash-malloc              \
            --with-installed-readline && make && make install 
exec /usr/bin/bash --login +h
tar -xpf libtool-2.4.6.tar.xz && cd libtool-2.4.6
./configure --prefix=/usr && make && make install && rm -fv /usr/lib/libltdl.a
make distclean
CC="gcc -m32" ./configure \
    --host=i686-pc-linux-gnu \
    --prefix=/usr            \
    --libdir=/usr/lib32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
make distclean
CC="gcc -mx32" ./configure \
    --host=x86_64-pc-linux-gnux32 \
    --prefix=/usr                 \
    --libdir=/usr/libx32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -rf libcap-2.4.6
tar xf gdbdm-1.22.tar.gz && cd gdbm-1.22
./configure --prefix=/usr    \
            --disable-static \
            --enable-libgdbm-compat && make && make install && cd .. && rm -Rf gdbdm-1.22
tar -xpf gperf-3.1.tar.gz && cd gperf-3.1
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1 && make && make install
cd .. && rm -rf gperf-3.1
tar -xpf expat-2.4.1.tar.xz && cd expat-2.4.1
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/expat-2.4.1 && make && make install
sed -e "/^am__append_1/ s/doc//" -i Makefile
make clean
CC="gcc -m32" ./configure \
    --prefix=/usr         \
    --libdir=/usr/lib32  \
    --host=i686-pc-linux-gnu && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR
sed -e "/^am__append_1/ s/doc//" -i Makefile
make clean
CC="gcc -mx32" ./configure \
    --prefix=/usr         \
    --libdir=/usr/libx32 \
    --host=x86_64-pc-linux-gnux32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf expat-2.4.1
tar -xpf inetutil-2.2.tar.xz && cd inetutils-2.2
./configure --prefix=/usr        \
            --bindir=/usr/bin    \
            --localstatedir=/var \
            --disable-logger     \
            --disable-whois      \
            --disable-rcp        \
            --disable-rexec      \
            --disable-rlogin     \
            --disable-rsh        \
            --disable-servers && make && make install
mv -v /usr/{,s}bin/ifconfig && cd .. && rm -Rf inetutils-2.2
tar -xpf less-590.tar.gz && cd less-590
./configure --prefix=/usr --sysconfdir=/etc && make && make install
cd .. && rm -Rf less-590
tar -xpf perl-5.34.0.tar.xz && cd perl-5.34.0
patch -Np1 -i ../perl-5.34.0-upstream_fixes-1.patch
export BUILD_ZLIB=False
export BUILD_BZIP2=0
sh Configure -des                                         \
             -Dprefix=/usr                                \
             -Dvendorprefix=/usr                          \
             -Dprivlib=/usr/lib/perl5/5.34/core_perl      \
             -Darchlib=/usr/lib/perl5/5.34/core_perl      \
             -Dsitelib=/usr/lib/perl5/5.34/site_perl      \
             -Dsitearch=/usr/lib/perl5/5.34/site_perl     \
             -Dvendorlib=/usr/lib/perl5/5.34/vendor_perl  \
             -Dvendorarch=/usr/lib/perl5/5.34/vendor_perl \
             -Dman1dir=/usr/share/man/man1                \
             -Dman3dir=/usr/share/man/man3                \
             -Dpager="/usr/bin/less -isR"                 \
             -Duseshrplib                                 \
             -Dusethreads && make && make install && unset BUILD_ZLIB BUILD_BZIP2
cd .. && rm -Rf perl-5.34.0
tar -xpf XML-Parser-2.46.tar.gz && cd XML-Parser-2.46
perl Makefile.PL && make && make install && cd .. && rm -Rf XML-Parser-2.46
tar -xpf intltool-51.0.tar.gz && cd intltool-5.10
sed -i 's:\\\${:\\\$\\{:' intltool-update.in
./configure --prefix=/usr && make && make install
cd .. && rm -Rf intltool-0.51.0
tar -xpf autoconf-2.71.tar.xz && cd autoconf-2.71
./configure --prefix=/usr && make && make install && cd .. && rm -Rf autoconf-2.71
tar -xpf automake-1.16.5.tar.xz && cd automake-1.16.5
./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.5 && make && make install
cd .. && rm -rf automake-1.16.5
tar -xpf kmod-29.tar.xz && cd kmod-29
./configure --prefix=/usr          \
            --sysconfdir=/etc      \
            --with-xz              \
            --with-zstd            \
            --with-zlib && make && make install
make install

for target in depmod insmod modinfo modprobe rmmod; do
  ln -sfv ../bin/kmod /usr/sbin/$target
done

ln -sfv kmod /usr/bin/lsmod && sed -e "s/^CLEANFILES =.*/CLEANFILES =/" -i man/Makefile
make clean
CC="gcc -m32" ./configure \
    --host=i686-pc-linux-gnu      \
    --prefix=/usr                 \
    --libdir=/usr/lib32           \
    --sysconfdir=/etc             \
    --with-xz                     \
    --with-zlib                   \
    --with-rootlibdir=/usr/lib32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && sed -e "s/^CLEANFILES =.*/CLEANFILES =/" -i man/Makefile
make clean
CC="gcc -mx32" ./configure \
    --host=x86_64-pc-linux-gnux32 \
    --prefix=/usr                 \
    --libdir=/usr/libx32          \
    --sysconfdir=/etc             \
    --with-xz                     \
    --with-zlib                   \
    --with-rootlibdir=/usr/libx32 && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf kmod-29
tar -xpf elfutils-0.186.tar.bz2 && cd elfutils-0.186
./configure --prefix=/usr                \
            --disable-debuginfod         \
            --enable-libdebuginfod=dummy && make
make -C libelf install
install -vm644 config/libelf.pc /usr/lib/pkgconfig
rm /usr/lib/libelf.a && make distclean
CC="gcc -m32" ./configure \
    --host=i686-pc-linux-gnu \
    --prefix=/usr            \
    --libdir=/usr/lib32      \
    --disable-debuginfod     \
    --enable-libdebuginfod=dummy && make
make DESTDIR=$PWD/DESTDIR -C libelf install
install -vDm644 config/libelf.pc DESTDIR/usr/lib32/pkgconfig/libelf.pc
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" ./configure \
    --host=x86_64-pc-linux-gnux32 \
    --prefix=/usr                 \
    --libdir=/usr/libx32          \
    --disable-debuginfod          \
    --enable-libdebuginfod=dummy && make
make DESTDIR=$PWD/DESTDIR -C libelf install
install -vDm644 config/libelf.pc DESTDIR/usr/libx32/pkgconfig/libelf.pc
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && rm -Rf elfutils-0.186
tar -xpf libffi-3.4.2.tar.gz && cd libffi-3.4.2
./configure --prefix=/usr          \
            --disable-static       \
            --with-gcc-arch=native \
            --disable-exec-static-tramp && make && make install && make distclean
CC="gcc -m32" CXX="g++ -m32" ./configure \
    --host=i686-pc-linux-gnu \
    --prefix=/usr            \
    --libdir=/usr/lib32      \
    --disable-static         \
    --with-gcc-arch=i686     \
    --disable-exec-static-tramp && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" CXX="g++ -mx32" ./configure \
    --host=x86_64-unknown-linux-gnux32 \
    --prefix=/usr            \
    --libdir=/usr/libx32     \
    --disable-static         \
    --with-gcc-arch=x86_64   \
    --disable-exec-static-tramp && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && rm -rf libffi-3.4.2
tar -xpf openssl-1.1.1l.tar.gz && cd openssl-1.1.1l.tar.gz
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic && make
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install
mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1l
make distclean
MACHINE="i686"             \
CC="gcc -m32 -march=i686"  \
CXX="g++ -m32 -march=i686" \
    ./config               \
    --prefix=/usr          \
    --openssldir=/etc/ssl  \
    --libdir=lib32         \
    shared                 \
    zlib-dynamic && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
MACHINE="x86_64"          \
CC="gcc -mx32"            \
CXX="g++ -mx32"           \
    ./config              \
    --prefix=/usr         \
    --openssldir=/etc/ssl \
    --libdir=libx32       \
    shared                \
    zlib-dynamic && make
make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -Rf openssl-1.1.1i
tar -xpf Python-3.10.0.tar.xz && cd Python-3.10.0
./configure --prefix=/usr        \
            --enable-shared      \
            --with-system-expat  \
            --with-system-ffi    \
            --with-ensurepip=yes \
            --enable-optimizations && make && make install && cd .. && rm -Rf Python-3.10.0
tar -xpf ninja-1.10.2.tar.gz && cd ninja-1.10.2
export NINJAJOBS=8
sed -i '/int Guess/a \
  int   j = 0;\
  char* jobs = getenv( "NINJAJOBS" );\
  if ( jobs != NULL ) j = atoi( jobs );\
  if ( j > 0 ) return j;\
' src/ninja.cc
python3 configure.py --bootstrap
install -vm755 ninja /usr/bin/
install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion  /usr/share/zsh/site-functions/_ninja
cd .. && rm -Rf ninja-1.10.2
tar -xpf meson-0.60.2.tar.gz && cd meson-0.60.2
python3 setup.py build
python3 setup.py install --root=dest
cp -rv dest/* /
install -vDm644 data/shell-completions/bash/meson /usr/share/bash-completion/completions/meson
install -vDm644 data/shell-completions/zsh/_meson /usr/share/zsh/site-functions/_meson && cd .. && rm -Rf meson-0.60.2
cd .. && rm -Rf meson-0.60.2
tar -xpf coreutils-9.0.tar.xz && cd coreutils-9.0
patch -Np1 -i ../coreutils-9.0-i18n-1.patch

autoreconf -fiv
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr            \
            --enable-no-install-program=kill,uptime && make && make install
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/' /usr/share/man/man8/chroot.8 && cd .. && rm -rf coreutils-9.0
tar -xpf check-0.15.2.tar.gz && check-0.15.2
./configure --prefix=/usr --disable-static
make && make docdir=/usr/share/doc/check-0.15.2 install
cd .. && rm -Rf check-0.15.2
tar xf diffutils-3.8.tar.xz && cd diffutils-3.8
./configure --prefix=/usr && make && make install && cd .. && rm -Rf diffutils-3.8
tar -xf gawk-5.1.1.tar.xz && cd gawk-5.1.1
sed -i 's/extras//' Makefile.in
./configure --prefix=/usr && make && make install && cd .. && rm -Rf gawk-5.1.1
tar -xf findutils-4.8.0 && cd findutils-4.8.0
case $(uname -m) in
    i?86)   TIME_T_32_BIT_OK=yes ./configure --prefix=/usr --localstatedir=/var/lib/locate ;;
    x86_64) ./configure --prefix=/usr --localstatedir=/var/lib/locate ;;
esac
make && make install && cd .. rm -Rf findutils-4.8.0
tar -xpf groff-1.22.4.tar.gz && cd groff-1.22.4
PAGE=letter ./configure --prefix=/usr && make -j1 && make install && cd .. && rm -Rf groff-1.22.4
cp /files/freetype-2.11.1.tar.xz /sources/ && tar-xpf freetype-2.11.1.tar.xz && cd freetype-2.11.1
sed -ri "s:.*(AUX_MODULES.*valid):\1:" modules.cfg &&

sed -r "s:.*(#.*SUBPIXEL_RENDERING) .*:\1:" \
    -i include/freetype/config/ftoption.h  &&

./configure --prefix=/usr --enable-freetype-config --disable-static &&
make && make install && cd .. && rm -Rf freetype-2.11.1
cp /files/popt-1.18.tar.gz /sources/ && tar -xpf popt-1.18.tar.gz
cd popt-1.8 && ./configure --prefix=/usr --disable-static &&
make && make install && cd .. && rm -Rf popt-1.18
cp /files/efivar-37* /sources/ && tar -xpf efivar-37.tar.bz2
cd efivar-37 && patch -Np1 -i ../efivar-37-gcc_9-1.patch
make CFLAGS="-O2 -Wno-stringop-truncation"
make install LIBDIR=/usr/lib && cd .. && rm -Rf efivar-37
cp /files/efibootmgr-17.tar.gz /sources/ && tar -xpf efibootmgr-17.tar.gz
cd efibootmgr-17 && sed -e '/extern int efi_set_verbose/d' -i src/efibootmgr.c
make EFIDIR=LFS EFI_LOADER=grubx64.efi
make install EFIDIR=LFS && cd .. && rm -Rf efibootmgr-17
cp /files/unifont-13.0.06.pcf.gz /sources/ && mkdir -pv /usr/share/fonts/unifont &&
gunzip -c ../unifont-13.0.06.pcf.gz > /usr/share/fonts/unifont/unifont.pcf
tar -xpf grub-2.06.tar.xz && cd grub-2.06
./configure --prefix=/usr        \
            --sysconfdir=/etc    \
            --disable-efiemu     \
            --enable-grub-mkfont \
            --with-platform=efi  \
            --disable-werror     &&
make && make install && mv -v /etc/bash_completion.d/grub /usr/share/bash-completion/completions
tar -xpf gzip-1.11.tar.xz && cd gzip-1.11
./configure --prefix=/usr && make && make install && cd ..
tar -xpf iproute2-5.15.0.tar.xz && cd iproute2-5.15.0
sed -i /ARPD/d Makefile
rm -fv man/man8/arpd.8
sed -i 's/.m_ipt.o//' tc/Makefile
make && make SBINDIR=/usr/sbin install
cd .. && rm -Rf iproute2-5.15.0
tar -xpf kbd-2.4.0.tar.xz && cd kbd-2.4.0
patch -Np1 -i ../kbd-2.4.0-backspace-1.patch
sed -i '/RESIZECONS_PROGS=/s/yes/no/' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
./configure --prefix=/usr --disable-vlock && make && make install && cd ..
rm -Rf kbd-2.4.0
tar -xpf-libpipeline-1.5.4.tar.gz && cd libpipeline-1.5.4
./configure --prefix=/usr && make && make install && cd .. && rm -Rf libpipeline-1.5.4
tar -xf make-4.3.tar.gz && cd make-4.3
./configure --prefix=/usr && make && make install && cd .. rm -Rf make-4.3
tar -xpf patch-2.7.6.tar.xz && cd patch-2.7.6
./configure --prefix=/usr && make && make install && cd .. && rm -Rf patch-2.7.6
tar -xf tar-1.34.tar.xz && cd tar-1.34
FORCE_UNSAFE_CONFIGURE=1  \
./configure --prefix=/usr && make && make install && cd .. && rm -Rf tar-1.34
tar -xpf texinfo-6.8.tar.xz && cd texinfo-6.8
./configure --prefix=/usr && sed -e 's/__attribute_nonnull__/__nonnull/' \
    -i gnulib/lib/malloc/dynarray-skeleton.c && make && make install
cd .. && rm -Rf texinfo-6.8
tar -xpf vim-8.2.3704.tar.gz && cd vim8.2.3704
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
./configure --prefix=/usr && make && make install
ln -sv vim /usr/bin/vi
for L in  /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done
ln -sv ../vim/vim82/doc /usr/share/doc/vim-8.2.3704
cp /files/etc/vimrc /etc/
cp /files/nano-5.9.tar.xz /sources/ && tar -xpf nano-5.9.tar.xz && cd nano-5.9
./configure --prefix=/usr     \
            --sysconfdir=/etc \
            --enable-utf8     \
            --docdir=/usr/share/doc/nano-5.9 &&
make && make install && cd .. && rm -Rf nano-5.9
cp /files/etc/nanorc /etc/
tar -xf eudev-3.2.10.tar.gz && cd eudev-3.2.10
./configure --prefix=/usr           \
            --bindir=/usr/sbin      \
            --sysconfdir=/etc       \
            --enable-manpages       \
            --disable-static && make && mkdir -pv /usr/lib/udev/rules.d
mkdir -pv /etc/udev/rules.d && make install &&
tar -xvf ../udev-lfs-20171102.tar.xz
make -f udev-lfs-20171102/Makefile.lfs install && make distclean
CC="gcc -m32" \
./configure --host=i686-pc-linux-gnu       \
            --prefix=/usr                  \
            --bindir=/usr/sbin             \
            --libdir=/usr/lib32            \
            --sysconfdir=/etc              \
            --disable-manpages             \
            --disable-static               \
            --config-cache && make && make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32
rm -rf DESTDIR && make distclean
CC="gcc -mx32" \
./configure --host=x86_64-lfs-linux-gnux32 \
            --prefix=/usr                  \
            --bindir=/usr/sbin             \
            --libdir=/usr/libx32           \
            --sysconfdir=/etc              \
            --disable-manpages             \
            --disable-static               \
            --config-cache && make && make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && udevadm hwdb --update && cd .. && rm -rf eudev-3.2.10
tar -xf man-db.2.9.4tar.xz && cd man-db-2.9.4
make && make install && rm -rf man-db-2.9.4
cp /files/linux-firmware-202111027.tar.gz /sources/ && tar -xpf linux-firmware-202111027.tar.gz 
cd linux-firmware-202111027 && make install && cd ..
tar -xpf procps-ng-3.3.17.tar.xz && cd procps-3.3.17
./configure --prefix=/usr                            \
            --docdir=/usr/share/doc/procps-ng-3.3.17 \
            --disable-static                         \
            --disable-kill && make && make install && cd .. && rm -rf procps-3.3.17
tar -xpf util-linux-2.37.2.tar.xz && cd util-linux-2.37.2
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --libdir=/usr/lib    \
            --docdir=/usr/share/doc/util-linux-2.37.2 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir \
            runstatedir=/run && make && make install && make distclean
mv /usr/bin/ncursesw6-config{,.tmp}
CC="gcc -m32" \
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --host=i686-pc-linux-gnu \
            --libdir=/usr/lib32      \
            --docdir=/usr/share/doc/util-linux-2.37.2 \
            --disable-chfn-chsh      \
            --disable-login          \
            --disable-nologin        \
            --disable-su             \
            --disable-setpriv        \
            --disable-runuser        \
            --disable-pylibmount     \
            --disable-static         \
            --without-python         \
            --without-systemd        \
            --without-systemdsystemunitdir && mv /usr/bin/ncursesw6-config{.tmp,}
make && make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/lib32/* /usr/lib32 && make distclean
mv /usr/bin/ncursesw6-config{,.tmp}
CC="gcc -mx32" \
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --host=x86_64-pc-linux-gnux32 \
            --libdir=/usr/libx32 \
            --docdir=/usr/share/doc/util-linux-2.37.2 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir && mv /usr/bin/ncursesw6-config{.tmp,}
&& make make DESTDIR=$PWD/DESTDIR install
cp -Rv DESTDIR/usr/libx32/* /usr/libx32
rm -rf DESTDIR && cd .. && rm -rf util-linux-2.37.2
tar -xf e2fsprogs-1.46.4.tar.gz && cd e2fsprogs-1.46.4
mkdir -v build
cd       build
../configure --prefix=/usr           \
             --sysconfdir=/etc       \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck && make && make install
rm -fv /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
gunzip -v /usr/share/info/libext2fs.info.gz
install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info && cd ..
rm -rf e2fsprogs-1.46.4
tar -xpf sysklogd-1.5.1.tar.gz && cd sysklog-1.5.1
sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
sed -i 's/union wait/int/' syslogd.c
make && make BINDIR=/sbin install && cd .. && rm -Rf sysklogd-1.5.1
tar -xpf sysvinit-3.00 && cd sysvinit-3.00
patch -Np1 -i ../sysvinit-3.00-consolidated-1.patch
make && make install && cd .. && rm -rf sysvinit-3.00

