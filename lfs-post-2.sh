chroot "$LFS" /usr/bin/env -i          \
    HOME=/root TERM="$TERM"            \
    PS1='(lfs chroot) \u:\w\$ '        \
    PATH=/usr/bin:/usr/sbin            \
    /usr/bin/bash --login
cd /sources/ && tar -xpf lfs-bootscripts-20210608.tar.xz && cd lfs-bootscripts-20210608
make install && cd ..
bash /usr/lib/udev/init-net-rules.sh
cat /etc/udev/rules.d/70-persistent-net.rules
cp -r /files/etc/sysconfig /etc/ && cp /files/etc/sysconfig/ifconfig.eth0
cp /files/etc/inittab /etc/
cp /files/etc/sysconfig/clock /etc/sysconfig/
cp /files/etc/inputrc /etc/
cp /files/etc/shells /etc/
cp /files/etc/fstab /etc/
cp /files/etc/profile /etc/
mkdir /mnt/windows
tar -xpf linux-5.15.5.tar.xz && cd linux-5.15.5
wget https://gitlab.com/bfitzgit23/lfs-scripts/-/raw/main/.config -O .config
make -j8 && make modules_install && cp -iv arch/x86/boot/bzImage /boot/vmlinuz-5.15.5-lfs-r11.0-312
cp -iv System.map /boot/System.map-5.15.5
cp -iv .config /boot/config-5.15.5
install -d /usr/share/doc/linux-5.15.5
cp -r Documentation/* /usr/share/doc/linux-5.15.5
install -v -m755 -d /etc/modprobe.d
cp /files/etc/modprobe.d/usb.conf /etc/modprobe.d/
grub-install --bootloader-id=LFS --recheck
cp /files/boot/grub/grub.cfg /boot/grub/
echo r11.0-312 > /etc/lfs-release
cp /files/etc/lsb-release /files/etc/os-release /etc/


