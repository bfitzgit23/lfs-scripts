#!/bin/bash

export LFS=/mnt/lfs
mkfs.ext4 -L "LFS" /dev/sdb3
mkdir -pv $LFS
mount -v -t ext4 /dev/sdb3 $LFS
mkdir -pv $LFS/boot/efi
mount /dev/sdb1 $LFS/boot/efi
mkdir /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile
cd $LFS
wget https://download2260.mediafire.com/tq34d0hytc3g/y9fne1jffkaw8eu/lfs-temp-tools-r11.0-306.tar.xz -P $LFS
tar -xpf $LFS/lfs-temp-tools-r11.0-306.tar.xz
mount -v --bind /dev $LFS/dev
mount -v --bind /dev/pts $LFS/dev/pts
mount -vt proc proc $LFS/proc
mount -vt sysfs sysfs $LFS/sys
mount -vt tmpfs tmpfs $LFS/run
chroot "$LFS" /usr/bin/env -i   \
    HOME=/root                  \
    TERM="$TERM"                \
    PS1='(lfs chroot) \u:\w\$ ' \
    PATH=/bin:/usr/bin:/sbin:/usr/sbin \
    /bin/bash --login +h

